# k8s-demo



## Getting started

Some links to help you get started with kubernetes:
- [k8s official docs](https://kubernetes.io/docs/home/)
- [k8s components](https://kubernetes.io/docs/concepts/overview/components/)
- [Kelsey Hightower K8s Talk from Puppet conf](https://www.youtube.com/watch?v=HlAXp0-M6SY)
- [K3s for self hosting k8s](https://k3s.io/)
